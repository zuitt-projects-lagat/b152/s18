console.log("Hello javascript objects");

//Array - a collection of related data
const grades = [91, 92, 93, 94, 95];
const names = ["Joy", "Natalia", "Biance"];

//Objects
	// - collection of multiple values with different data types
const objGrades = {
	//property: value (key-value pairs)
	firstName: "Aaron",
	lastName: "Delos Santos",
	firstGrading: 91,
	subject: "English",
	teachers: ["Carlo","Mashiro"],
	isActive: true,
	schools: {
		city: "Manila",
		country: "Philippines"
	},
	studentNames: [
		{
			name: "Adrian",
			batch: "152"
		},
		{
			name: "Nikko",
			batch: "152"
		}
	],
	description: function(){
		return `${this.subject}: ${this.firstGrading} of students ${this.studentNames[0].name} and ${this.studentNames[1].name}`
	}
}

//How do we acces properties of an object?
	//dot notation (.)
	//bracket notation ([""])
/*
	syntax:

	objReference.propertyName
	objReference["propertyName"]

*/
console.log(objGrades.firstGrading);	//91
console.log(objGrades.subject);			//English
console.log(objGrades["isActive"]);
console.log(objGrades["teachers"]);
console.log(objGrades["firstName"]);


console.log(objGrades.description());

//In schools property, access country property
console.log(objGrades.schools.country);
console.log(objGrades["schools"]["city"]);


//In studentNames property, access the second element
console.log(objGrades.studentNames[1]);
console.log(objGrades.studentNames[1].name);
console.log(objGrades.studentNames[1]["batch"]);

//Q: Is it possible to add a new property in an object?
objGrades.semester = "first";

// console.log(objGrades);


// Is it possible to delete a property in an object? -Yes
delete objGrades.semester;
//console.log(objGrades);


//Mini-Activity:
const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];

//Solution #1: using manual computation

/*
//first element
let ave1 = (studentGrades[0].Q1 + studentGrades[0].Q2 + studentGrades[0].Q3 + studentGrades[0].Q4) / 4
console.log(ave1);
studentGrades[0].average = parseFloat(ave1.toFixed(1));
//console.log(studentGrades);

//second element
let ave2 = (studentGrades[1].Q1 + studentGrades[1].Q2 + studentGrades[1].Q3 + studentGrades[1].Q4) / 4
console.log(ave2);
studentGrades[1].average = parseFloat(ave2.toFixed(1));
//console.log(studentGrades);

//third element:
let ave3 = (studentGrades[2].Q1 + studentGrades[2].Q2 + studentGrades[2].Q3 + studentGrades[2].Q4) / 4
console.log(ave3);
studentGrades[2].average = parseFloat(ave3.toFixed(1));
//console.log(studentGrades);

//fourth element:
let ave4 = (studentGrades[3].Q1 + studentGrades[3].Q2 + studentGrades[3].Q3 + studentGrades[3].Q4) / 4
console.log(ave4);
studentGrades[3].average = parseFloat(ave4.toFixed(1));
//console.log(studentGrades);

//fifth element:
let ave5 = (studentGrades[4].Q1 + studentGrades[4].Q2 + studentGrades[4].Q3 + studentGrades[4].Q4) / 4
console.log(ave5);
studentGrades[4].average = parseFloat(ave5.toFixed(1));
console.log(studentGrades);
*/

//Solution #2: using for loop

/*
for(let i = 0; i < studentGrades.length; i++){

	let ave = (studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 + studentGrades[i].Q4) / 4

	console.log(ave)

	studentGrades[i].average = parseFloat(ave.toFixed(1))
}

console.log(studentGrades);
*/


//Solution #3: using forEach method

studentGrades.forEach(function(element){
	console.log(element)

	let ave = (element.Q1 + element.Q2 + element.Q3 + element.Q4) / 4

	console.log(ave)

	element.average = parseFloat(ave.toFixed(1))
})

//console.log(studentGrades);



/* Object Constructor

*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	attack: function(){
		console.log(`This Pokemon tackled targetPokemon`)
		console.log(`targetPokemon's health is now reduced to targetPokemonhealth`)
	},
	faint: function(){
		console.log(`Pokemon fainted`)
	}
}
console.log(myPokemon);

function Pokemon(name, lvl, hp){
	this.name = name,
	this.level = lvl,
	this.health = hp * 2
	this.attack = lvl,
	this.tackle = function(target){

		console.log(`${this.name} tackled ${target.name}`)
		//Using compound assigment operator
		target.health -= this.attack
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
		if (target.health <= 10){
			target.faint();
		}
	},
	this.faint = function(){
		if (this.health < 10){
		console.log(`Pokemon fainted`)
		}
	}
}

let pikachu = new Pokemon("Pikachu", 10, 50);
let charizard = new Pokemon("Charizard", 8, 40);
let squirtle = new Pokemon("Squirtle", 10, 10);


console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));
console.log(pikachu.tackle(charizard));

/*Review*/

/*Creating an object with object literals*/

/*{} vs [] - {objectLiteral} [arrayLiteral]*/

let object1 = {
	property: "sample"
};

let array1 = ["sample","sample1"];

console.log(typeof object1);
console.log(typeof array1);

//What do we use to check the type of data? typeof keyword
/*Arrays are a special type of data/object.*/

let object2 = {}

console.log({} === object2)

/*
	Any instance of an object is unique. No two objects are the same and thus we cannot use the === strict equality comparison on the objects themselves, however we can compare their properties.
*/

let object3 = {
	name: "sample2"
}

let object4 = {
	name: "sample2"
}

console.log(object3 === object4)
console.log(object3.name === object4.name);
 
/*Creating an object with constructor*/

function Dog(name,breed,age){

	/*this keyword refers to the object you will create with your constructor*/
	/*this.property = value*/
	this.name = name;
	this.breed = breed;
	this.age = age * 7;
	/*Can we also add methods to constructor?*/
	/*yes.*/
	this.bark = function(){
		console.log("Woof!")
	}
}

/*new keyword to create a new object out of our constructor.*/
/*If you don't use the new keyword, the object will not be created*/
let dog1 = new Dog("Bolt","Corgi",5);
console.log(dog1);

/*
	Conventions when creating our constructor:

		To distinguish it with other functions we usually capitalize the name of our constructor.

*/