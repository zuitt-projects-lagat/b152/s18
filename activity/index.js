const trainer = {
	name: 'Nikko Lagat',
	age: 22,
	pokemon: ['Mudkip','Phantump','Trubbish','Stonjourner'],
	friends: {
		hoenn: ['Wally','Brawly','Roxanne'],
		unova: ['Chilli','Elesa','Skyla'],
		galar: ['Hop','Bede','Nessa']
	},
	talk: function(){
		console.log('Mudkip! I choose you!')
	}
}

console.log(trainer);


console.log('Result of dot notation:');
console.log(trainer.name);
console.log(trainer.friends);

console.log('Result of bracket notation:');
console.log(trainer["pokemon"]);
console.log(trainer["friends"]["galar"]);

console.log('Result of talk method:');
console.log(trainer.talk());


function Pokemon(name, lvl){
	this.name = name,
	this.level = lvl,
	this.health = lvl * 10,
	this.attack = lvl * 2,
	this.tackle = function(opp){

		console.log(`${this.name} tackled ${opp.name}`)
		opp.health -= this.attack

		console.log(`${opp.name}'s health is now reduced to ${opp.health}`);
		if (opp.health <= 0){
			opp.faint();
		}
	},
	this.faint = function(){
		console.log(`${this.name} has fainted.`)
	}
}

let mudkip = new Pokemon("Mudkip", 11);
console.log(mudkip);
let bidoof = new Pokemon("Bidoof", 8);
console.log(bidoof);


console.log(mudkip.tackle(bidoof));
console.log(mudkip.tackle(bidoof));
console.log(mudkip.tackle(bidoof));
console.log(mudkip.tackle(bidoof));
console.log(bidoof)


console.log("New Battle Scene:")
let phantump = new Pokemon("Phantump", 9);
console.log(phantump);
let pidgey = new Pokemon("Pidgey", 4);
console.log(pidgey);

console.log(phantump.tackle(pidgey));
console.log(phantump.tackle(pidgey));
console.log(phantump.tackle(pidgey));
console.log(pidgey);